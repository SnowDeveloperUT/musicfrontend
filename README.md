# README #

* Subscription Info Client (SIC) - User front end

### Technologies used: ###
* Front-end:
	* Html/CSS, 
	* JavaScript, 
	* JQuery, 
	* Bootstrap, 
	* Angular 2 and some JS plugins,
	* Thymeleaf
	* Spring Framework

* Features
	* Secure login
	* Responsive bootstrap we application
	* Session management for secure access
	* Role based access - Role of a User/Role of admin
	* Signup and login validations

### How do I get set up? ###

* Summary of set up - Spring framework
* Database configuration - MYSQL 
	* Create a new database called MusicSubscription
	* Add two roles with id '0', '1' and names 'ROLE_USER', 'ROLE_ADMIN'
	* The frontend can now be run (UserfrontApplication.java) with any IDE such as IntelliJ
