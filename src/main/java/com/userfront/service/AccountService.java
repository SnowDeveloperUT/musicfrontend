package com.userfront.service;

import com.userfront.domain.MusicAccount;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface AccountService {
	//Creating music account
	MusicAccount createMusicAccount();
}
