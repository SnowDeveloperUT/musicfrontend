package com.userfront.service.UserServiceImpl;

import com.userfront.dao.UserDao;
import com.userfront.domain.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 * Created by snowwhite on 7/16/2017.
 */
@Service
public class UserSecurityService implements UserDetailsService {
	//Application logger
	private static final Logger LOG = LoggerFactory.getLogger(UserSecurityService.class);

	@Autowired
	private UserDao userDao;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userDao.findByUsername(username);
		System.out.printf("username security:" + username);
		if (null == user) {
			LOG.warn("Phone {} not found", username);
			throw new UsernameNotFoundException("Phone " + username + " not found");
		}
		return user;
	}
}
