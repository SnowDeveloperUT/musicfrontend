package com.userfront.service.UserServiceImpl;

import com.userfront.dao.MusicAccountDao;
import com.userfront.dao.SubscriptionDao;
import com.userfront.domain.MusicAccount;
import com.userfront.domain.Subscription;
import com.userfront.service.AccountService;
import com.userfront.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Random;

/**
 * Created by snowwhite on 7/16/2017.
 */
@Service
public class AccountServiceImpl implements AccountService {
	private static int nextAccountNumber = 11223145;

	@Autowired
	private MusicAccountDao musicAccountDao;

	@Autowired
	private SubscriptionDao subscriptionDao;

	@Autowired
	private UserService userService;

	public MusicAccount createMusicAccount() {

		//Generating random numbers for unique account numbers of the user
		Random rand = new Random();
		int  n = rand.nextInt(50000) + 1;

		//Whenever user signs up then their subscription is activated
		//The current date is taken to be their start date of subscription
		LocalDateTime dateThisMonth = LocalDateTime.now();

		//Evaluation of next month and showing of last date of subscription
		LocalDateTime dateNextMonth = dateThisMonth.plusMonths(1);
		LocalDateTime oneLessDayofNextMonth = dateNextMonth.minusDays(1);

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMM dd, yyyy");
		String sdateThisMonth = dtf.format(dateThisMonth);
		String sdateNextMonth = dtf.format(dateNextMonth);

		System.out.println("dateThisMonth: " + sdateThisMonth);
		System.out.println("dateNextMonth: " + sdateNextMonth);

		MusicAccount musicAccount = new MusicAccount();
		musicAccount.setAccountNumber(n);
		musicAccount.setNextBillingDate(sdateNextMonth.trim());
		musicAccount.setServiceName("Premium account for €2.99/month");
		musicAccount.setServiceProvider("GO Server");

		musicAccountDao.save(musicAccount);

		//Finding date of subscription with proper format
		DateTimeFormatter dtf1 = DateTimeFormatter.ofPattern("dd.MM.yy");
		String sdateThisMonth1= dtf1.format(dateThisMonth);
		String oneLessDayofNextMonth1 = dtf1.format(oneLessDayofNextMonth);

		Subscription subscription = new Subscription();
		subscription.setStartDate(sdateThisMonth1);
		subscription.setEndDate(oneLessDayofNextMonth1);
		subscription.setTotal(2.99);

		subscriptionDao.save(subscription);

		System.out.println("Music Account" + musicAccount);
		return musicAccountDao.findByAccountNumber(musicAccount.getAccountNumber());
	}


}
