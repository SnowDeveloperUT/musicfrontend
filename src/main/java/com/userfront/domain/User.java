package com.userfront.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.userfront.domain.security.Authority;
import com.userfront.domain.security.UserRole;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by snowwhite on 7/12/2017.
 */
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@Entity
public class User implements UserDetails{

	//Added all needed variables
	@Id
	@GeneratedValue
	@Column(name = "userId", nullable = false, updatable = false)
	private Long userID;

	private String username;//As phone number

	private String password;
	private String firstName;
	private String lastName;

	@Column(name = "email", nullable = false, unique = true)
	private String email;

	private boolean enabled=true; //Having a variable 'enabled' to specify if the user is an active user or not

	//One User can have one Music Account - Based on the phone number with which a person logs in
	@OneToOne
	private MusicAccount musicAccount;

	//Having two user roles - User( ROLE_USER) and Admin(ROLE_ADMIN)
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<UserRole> userRoles = new HashSet<>();

	//Getting the role of the logged in person - User or Admin
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> authorities = new HashSet<>();
		userRoles.forEach(ur -> authorities.add(new Authority(ur.getRole().getName())));
		return authorities;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}
}
