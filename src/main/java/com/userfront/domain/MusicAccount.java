package com.userfront.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.autoconfigure.domain.EntityScan;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Created by snowwhite on 7/12/2017.
 */
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@AllArgsConstructor(staticName = "of")
@Entity
public class MusicAccount {

	//Every Music Account will be having the following variables
	@Id
	@GeneratedValue
	private Long accountId;
	private int accountNumber;
	private String nextBillingDate;
	private String serviceName;
	private String serviceProvider;

	@OneToMany(mappedBy = "musicAccount", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JsonIgnore
	private List<Subscription> subscriptions;

}
