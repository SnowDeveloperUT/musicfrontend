package com.userfront.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by snowwhite on 7/12/2017.
 */
@Data
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(force = true, access = AccessLevel.PUBLIC)
@Entity
public class Subscription {

	//Every user that signs up will be automatically subscribed and
	//the subscription will have the following variables
	@Id
	@GeneratedValue
	private Long id;
	private String startDate;
	private String endDate;
	private double total;

	@ManyToOne
	@JoinColumn(name = "music_account_id")
	private MusicAccount musicAccount;


	public Subscription(String startDate, String endDate, double total) {
		this.startDate = startDate;
		this.endDate = endDate;
		this.total = total;
	}
}
