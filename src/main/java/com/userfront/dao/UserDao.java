package com.userfront.dao;

import com.userfront.domain.User;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface UserDao extends CrudRepository<User, Long> {
	//Identifying the user based on his name or email
	User findByUsername(String username);
	User findByEmail(String email);
}
