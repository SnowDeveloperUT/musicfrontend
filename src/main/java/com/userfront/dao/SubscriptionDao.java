package com.userfront.dao;

import com.userfront.domain.MusicAccount;
import com.userfront.domain.Subscription;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/17/2017.
 */
public interface SubscriptionDao extends CrudRepository<Subscription, Long> {
	//Subscription Dao
}
