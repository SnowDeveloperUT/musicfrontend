package com.userfront.dao;

import com.userfront.domain.MusicAccount;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface MusicAccountDao extends CrudRepository<MusicAccount, Long> {

	//Opening the user account page based on his/her account number
	MusicAccount findByAccountNumber (int accountNumber);

}
