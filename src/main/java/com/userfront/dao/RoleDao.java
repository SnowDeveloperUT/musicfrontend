package com.userfront.dao;

import com.userfront.domain.security.Role;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by snowwhite on 7/16/2017.
 */
public interface RoleDao extends CrudRepository<Role, Integer> {

	//Understanding the role of the user (Users/Admin) by their role name
	Role findByName(String name);
}
