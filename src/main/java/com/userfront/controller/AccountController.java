package com.userfront.controller;

import com.userfront.domain.MusicAccount;
import com.userfront.domain.User;
import com.userfront.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

/**
 * Created by snowwhite on 7/17/2017.
 */
@Controller
@RequestMapping("/account")
public class AccountController {
	//Music Account controller

	@Autowired
	private UserService userService;

	@RequestMapping("/musicAccount")
	public String musicAccount(Model model, Principal principal){

		User user = userService.findByUsername(principal.getName());
		MusicAccount musicAccount = user.getMusicAccount();

		model.addAttribute("musicAccount", musicAccount);

		return "musicAccount";
	}
}
