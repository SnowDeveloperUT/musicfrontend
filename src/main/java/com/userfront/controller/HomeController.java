package com.userfront.controller;

import com.userfront.dao.RoleDao;
import com.userfront.dao.UserDao;
import com.userfront.domain.MusicAccount;
import com.userfront.domain.User;
import com.userfront.domain.security.UserRole;
import com.userfront.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.lang.management.MemoryUsage;
import java.security.Principal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by snowwhite on 7/12/2017.
 */
@Controller
public class HomeController {

	@Autowired
	private UserService userService;

	@Autowired
	private RoleDao roleDao;

	@RequestMapping("/")
	public String home() {
		return "redirect:/index";
	}

	@RequestMapping("/index")
	public String index() {
		return "index";
	}

	//Get method for sign up
	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signup(Model model){
		User user = new User();
		model.addAttribute("user",user);
		return "signup";
	}

	//Post method after signup
	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupPost(@ModelAttribute("user") User user, Model model) {

		if(userService.checkUserExists(user.getUsername(), user.getEmail()))  {

			if (userService.checkEmailExists(user.getEmail())) {
				model.addAttribute("emailExists", true);
			}

			if (userService.checkUsernameExists(user.getUsername())) {
				model.addAttribute("usernameExists", true);
			}

			return "signup";
		} else {
			Set<UserRole> userRoles = new HashSet<>();
			userRoles.add(new UserRole(user, roleDao.findByName("ROLE_USER")));

			userService.createUser(user, userRoles);

			return "redirect:/";
		}
	}

	//User front end page
	@RequestMapping("/userfront")
	public String userFront(Principal principal, Model model) {
		User user = userService.findByUsername(principal.getName());
		System.out.println("username====== "+user);
		MusicAccount musicAccount = user.getMusicAccount();

		model.addAttribute("musicAccount", musicAccount);

		return "userfront";
	}
}
